# Scala Evaluation #

### Set Up ###

* Pull repo down
* Recommend using IntelliJ to run
    * There is an included run configuration

### Tasks ###

1.   Clean the input of duplicates

2.   Explode the data for missing days (assume date range of **2020-01-01 - 2020-12-31**)

3.   Produce a versioned property-by-date data frame from the original csv

4.   Zip the repo up and send it back

(2) Example:
```
property 1,123,123,2020-01-01,2020-01-31
property 1,150,123,2020-02-01,2020-05-31
property 1,150,150,2020-06-01,2020-12-31
property 2,120,120,2020-01-01,2020-12-31
property 3,120,120,2020-01-01,2020-02-29
property 3,120,130,2020-03-01,2020-12-31
```
should become
```
property 1,123,123,2020-01-01
property 1,123,123,2020-01-02
property 1,123,123,2020-01-03
property 1,123,123,2020-01-04
property 1,123,123,2020-01-05
...
property 1,150,123,2020-02-01
property 1,150,123,2020-02-02
property 1,150,123,2020-02-03
property 1,150,123,2020-02-04
...
property 1,150,123,2020-12-29
property 1,150,123,2020-12-30
property 1,150,123,2020-12-31
property 2,120,120,2020-01-01
...
property 2,120,120,2020-12-31
property 3,120,120,2020-01-01
...
property 3,120,130,2020-12-31
```

(3) Example:

*Using exploded data set*
```
property name,available rooms,available meeting space,change date,version
property 1,123,123,2020-01-01,1
property 1,150,123,2020-02-01,2
property 1,150,150,2020-06-01,3
property 2,120,120,2020-01-01,1
property 3,120,120,2020-01-01,1
property 3,120,130,2020-03-01,2
```

### Main File ###
```
src/main/scala/com/kalibri/eval/EvalRunner.scala
```

### Expected output ###

Two CSV files:

* **exploded.csv**
    * Exploded set of data for each day in 2020
* **versions.csv**
    * Versioned output of the original data