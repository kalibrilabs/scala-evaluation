package com.kalibri.csv

import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.types.StructType

sealed abstract class SparkReadMode(val name: String)

case object Permissive extends SparkReadMode("PERMISSIVE")

case object DropMalformed extends SparkReadMode("DROPMALFORMED")

case object Failfast extends SparkReadMode("FAILFAST")

/**
 * Class of options used for reading/writing data from CSV files
 *
 * @param schema                - Object schema of the data to read (can be null if the schema can be inferred)
 * @param header                - If the data header should be retrieved
 * @param delimiter             - Delimiter separating the data
 * @param timestampFormat       - Format to parse date/time with
 * @param quote                 - The character used for a quote
 * @param escape                - The character used to escape a quote
 * @param quoteAll              - If every field should be in quotes
 * @param errorMode             - Method of handling malformed records
 * @param saveMode              - Method of saving the file
 * @param multiLine             - If the reader should allow rows that span multiple lines
 * @param allowMalformedRecords - allow reading to continue if there are malformed records.
 */
case class CsvFileOptions(var schema: StructType = null,
                          header: Boolean = true,
                          delimiter: String = ",",
                          timestampFormat: String = "yyyy-MM-dd",
                          quote: String = "\"",
                          escape: String = "\"",
                          quoteAll: Boolean = true,
                          var errorMode: SparkReadMode = Failfast,
                          saveMode: SaveMode = SaveMode.Overwrite,
                          multiLine: Boolean = false,
                          compression: Boolean = false,
                          allowMalformedRecords: Boolean = false)